# Intro to 5/10/2020 Mishna Avot 4 Teaching

1. Shulamit Magnus introduced me to Mishna
1. Tradition to study Mishna in honor of someone who has recently died. 
1. 10 days ago the father of my best friend from childhood Walter Kornbluh passed away. And though David and I never lived in the same city since college, the friendship that had emerged between our parents continued to grow and flourish. Wally was full of enthusiasm, hope, and encouragement. I will miss him. Sarah and I dedicate this session in his honor. 
1. Magnus introduced me to the Kehati Mishna
1. Kehati:
    1. Born in Poland in 1910
    1. Studied in Zionist and religious schools
    1. Received ordination in 1933
    1. Made aliyah in 1935
    1. Initially studed at Hebrew U but dropped out for financial reasons
    1. Worked initially as a teacher and then as a bank teller
    1. In the wake of the Holocaust many religious Israelis began studying Mishna in honor of those lost. Kehati wanted to create a commentary in modern Hebrew that would be easy to read. At first he tried to recruit other people to join him in this effort, but in the end wrote the whole thing himself. It was completed and published in 1965. The first complete English edition was published in 1995.
