## Omer Avot Study of Germantown Jewish Centre

16th of Iyyar, 5780, 31st Day of the Omer (Yesod in Hod) corresponding to May 10, 2020
Presented by Sarah Braun and R. Shai Gluskin

### Pirkei Avot Chapter 4:1 | פרקי אבות פרק ד:א

![image of Hebrew text for mishna 4:1](images/pirkei-avot-4-1.png)

Ben Zoma said: Who is wise? They who learn from every person, as it is said, "From all the people who have taught me have I been enlightened."  *Psalms 119:99 (3)* 

Who is mighty? They who conquer their passions, as it is said "They who are slow to anger are better than the mighty; and they who rule their spirit than the one who takes a city by force." *Proverbs 16:32 (4)*

Who is wealthy? They who rejoice in their portion, as it is said, "When you eat the labor of your hands, happy shall you be, and it shall be well with you." *Psalms 128:2 (5)*
"Happy shall you be" - in this world. "And it shall be well with you" - in the world-to-come. 

Who is honored? They who honor other people, as it is said,  "for them who honor Me I will honor, and they who despise Me shall be lightly esteemed." *1 Samuel 2:30 (6)*

#### Associated Texts

##### Jeremiah 9

|                                                     יִרְמְיָהוּ ט | Jeremiah 9 Verses, 4,5,22,23                                 |
| -----------------------------------------------------------: | ------------------------------------------------------------ |
| ד  וְאִישׁ בְּרֵעֵהוּ יְהָתֵלּוּ, וֶאֱמֶת לֹא יְדַבֵּרוּ; לִמְּדוּ לְשׁוֹנָם דַּבֶּר-שֶׁקֶר, הַעֲוֵה נִלְאוּ | 4 And they deceive every one his neighbor, and truth they speak not; they have taught their tongue to speak lies, they weary themselves to commit iniquity. |
|            ה  שִׁבְתְּךָ, בְּתוֹךְ מִרְמָה; בְּמִרְמָה מֵאֲנוּ דַעַת-אוֹתִי, נְאֻם-יְהוָה | 5 Your habitation is in the midst of deceit; through deceit they refuse to know Me, says Adonai. |
| **כב  כֹּה אָמַר יְהוָה, אַל-יִתְהַלֵּל חָכָם בְּחָכְמָתוֹ, וְאַל-יִתְהַלֵּל הַגִּבּוֹר, בִּגְבוּרָתוֹ; אַל-יִתְהַלֵּל עָשִׁיר, בְּעָשְׁרוֹ** | **22 This says Adonai: Let not the wise man glory in his wisdom, neither let the mighty man glory in his might, let not the rich man glory in his riches;** |
| כג  כִּי אִם-בְּזֹאת יִתְהַלֵּל הַמִּתְהַלֵּל, הַשְׂכֵּל וְיָדֹעַ אוֹתִי--כִּי אֲנִי יְהוָה, עֹשֶׂה חֶסֶד מִשְׁפָּט וּצְדָקָה בָּאָרֶץ:  כִּי-בְאֵלֶּה חָפַצְתִּי, נְאֻם-יְהוָה | 23 But let him that glories in this kind of glorying: understanding Me, knowing Me; that I am Adonai who exercises mercy, justice, and righteousness in the land; For in these things I delight, says Adonai. |







##### Bartenura (15 century, Italy)

Forbearance is a virtue when it comes *migibbor* - from the spiritual strength of conquering one's inclinations, and ruling one's spirit has merit *miloked ir* - when it comes from the conqueror of a city; e.g., a victorious king who refrains from executing those who had rebelled against him.

##### Kehati (20th Century, Israel)

**Who is honored?** - having defined the nature of wisdom, might, and wealth, whereby a person gains the esteem of God and man, even though these qualities may not be acknowledged by all, Ben Zoma now declares what one must do to gain universal respect. **He who honors others** - the reverent treatment of others reflects honor upon one's own person, as it is said: "**for them who honor Me I will honor** - the verse refers to God, and *a fortiori* to man. For if God, Whose creatures were formed solely to venerate Him, reciprocates the honor displayed towards Him, then man must do so all the more, **and they who despise Me shall be lightly esteemed**" - *yekalu* (the passive form) indicates that this is a natural result of the contemptuous person's own baseness.

##### Leviticus 10, A Bridge to Avot 4:23

|                                                      וַיִּקְרָא י | Leviticus 10, verses 1-3                                     |
| -----------------------------------------------------------: | ------------------------------------------------------------ |
| א  וַיִּקְחוּ בְנֵי-אַהֲרֹן נָדָב וַאֲבִיהוּא אִישׁ מַחְתָּתוֹ, וַיִּתְּנוּ בָהֵן אֵשׁ, וַיָּשִׂימוּ עָלֶיהָ, קְטֹרֶת; וַיַּקְרִיבוּ לִפְנֵי יְהוָה, אֵשׁ זָרָה--אֲשֶׁר לֹא צִוָּה, אֹתָם | 1 And Nadav and Avihu, the sons of Aaron, took each of them his censer, and put fire therein, and laid incense thereon, and offered strange fire before Adonai, which He had not commanded them. |
|          ב  וַתֵּצֵא אֵשׁ מִלִּפְנֵי יְהוָה, וַתֹּאכַל אוֹתָם; וַיָּמֻתוּ, לִפְנֵי יְהוָה | 2 And there came forth fire facing Adonai, and devoured them, and they died facing Adonai. |
| ג  וַיֹּאמֶר מֹשֶׁה אֶל-אַהֲרֹן, הוּא אֲשֶׁר-דִּבֶּר יְהוָה לֵאמֹר בִּקְרֹבַי אֶקָּדֵשׁ, וְעַל-פְּנֵי כָל-הָעָם, אֶכָּבֵד; וַיִּדֹּם, אַהֲרֹן | 3 Then Moses said unto Aaron: 'This is it that Adonai spoke saying: Through them that are close to Me I will be sanctified, and on the face of all the people I will be honored.' Aaron was silent. |



### Pirkei Avot Chapter 4:23 - פרקי אבות פרק ד:כג

![image of Hebrew text for mishna 4:23](images/pirkei-avot-4-23.png)

Rabbi Shimon ben Eleazar said: Do not appease your friend at the hour of his anger.

Nor console him while his dead lie before him.

Nor question him at the time of his vow.

Nor strive to see him in the hour of his disgrace.









#### Pinchas Kehati, 20th Century; Israel

**Rabbi Shimon ben Eleazar said: Do not appease your friend at the hour of his anger** - because he will not accept your counsel and you may even raise his temper. Our Sages asked (*Ber*-. 7a): "Whence do we know that it is wrong to pacify a person when he is angry? Because it is written (Ex. 33: 14): 'My face shall go with you, and I shall give you rest' - God said to Moses: Wait until the anger has passed from my face, and I shall grant your request."

**Nor console him while his dead lie before him** - when no person is receptive to consolation.

**Nor question him at the time of his vow** - to find loopholes for a possible annulment of his vow because being incensed, and his mind set on his oath, he will insist on its unconditional implementation, and you will thereby forfeit any chance of later retraction.

**Nor strive to see him in the hour of his disgrace** - when he succumbs to sin, and wishes to escape notice. Thus, in the case of Adam, when he sinned, God did not address them (Adam and Havah) until after (Gen. 3:7): "they made themselves girdles," whereupon *(ibid*. 8): "they heard the voice of the Lord God."

|                                                   יבמות סה ב | Talmud Yevamot 65b                                           |
| -----------------------------------------------------------: | ------------------------------------------------------------ |
| כשם שמצוה על אדם לומר דבר הנשמע<br />כך מצוה על אדם שלא לומר דבר שאינו נשמע | Just as it is a mitzva for a person to say that which will be heeded, so is it a mitzva for a person not to say that which will not be heeded. |

 

------

### Resources

Pinchas Kehati Wikipedia Page: https://en.wikipedia.org/wiki/Pinchas_Kehati
Pinchas Kehati Mishnah Commentary in English (partial): https://bit.ly/kehati-english
Talmud Hebrew/Aramaic with English translation by R. Adin Steinsaltz,
	and so much more: https://www.sefaria.org

 